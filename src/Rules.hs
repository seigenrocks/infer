module Sequent
    ( Sequent (..)
    , Rule (..)
    )
    where

import Control.Arrow
import Data.List

import Language
import Derivation
import Tree



type RuleName = String

newtype Matcher a = Matcher { unMatcher :: MonadState State m => Sequent -> m (Maybe Sequent) }

data Rule =
    Rule
        RuleName
        (Matcher Sequent)
{-         (Matcher Expr)
        (Matcher Type)
 -}

attemptRule :: Rule -> Sequent -> Maybe Sequent
attemptRule (Rule _ match) sequent = match sequent


abstraction :: Rule
abstraction = Rule "λ-I" match
    where
        match (ctx, (Lam var expr), ty) = do
            (i, )
            let (Tarr)