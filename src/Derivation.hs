module Derivation
    ( Context
    , DerivationTree (..)
    , Node (..)
    , NodeState (..)
    , State (..)
    , freshTV
    , isIn
    )
    where

import Language
import Tree


type Context = [(Id, Type)]

isIn :: Id -> Type -> Context
isIn name ty = [(name, ty)]

type Sequent = (Context, Expr, Type)

data NodeState =  Pending | Ready | Done

data Node =
    Node { sequent :: Sequent
         , rule :: RuleName
         , state :: NodeState
         }

type DerivationTree = TreeZip Node

type State = (Int, DerivationTree, Subst)

freshTV :: MonadState State m => m Type
freshTV = do
    (i, d, s) <- get
    put (1+i, d, s)
    return "t" ++ show i

{- check that errithang is ready before hitting done or pending -}
{- check that done implies no further progress can be made along that branch -}
{- test rules -}