{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Language
    ( Expr (..)
    , Id
    , Subst
    , Type (..)
    , Types
    , (@@)
    , applySubst
    , emptySubst
    , freeVars
    , mgu
    )
    where

import Control.Arrow
import Control.Monad
import Data.Functor.Foldable
import Data.Functor.Foldable.TH
import Data.List


type Id = String


data Expr =
    Var Id |
    Num Int |
    Lam Id Expr |
    App Expr Expr |
    Let Id Expr Expr

makeBaseFunctor ''Expr


instance Show Expr where
    show = para alg
        where
            alg (VarF v) = v
            alg (NumF n) = show n
            alg e@(LamF v (_, exprF)) = "λ" ++ v ++ "." ++ exprF
            alg (AppF (expr1, exprF1) (expr2, exprF2)) =
                showApp expr1 exprF1 ++ " " ++ showApp expr2 exprF2
            alg (LetF v (_, exprF1) (_, exprF2)) =
                "let " ++ v ++ " = " ++ exprF1 ++ " in " ++ exprF2

            showApp (Lam _ _) exprF = "(" ++ exprF ++ ")"
            showApp _ exprF = exprF


data Type =
    Tnum |
    Tvar Id |
    Tarr Type Type |
    Forall [Id] Type

makeBaseFunctor ''Type


instance Show Type where
    show = para alg
        where
            alg TnumF = "num"
            alg (TvarF tv) = tv
            alg (TarrF (t1, tf1) (_, tf2)) = arrShow t1 tf1 ++ " → " ++ tf2
            alg (ForallF tvs (_, tf)) = "∀" ++ list tvs ++ "." ++ tf

            arrShow (Tarr _ _) tf = "(" ++ tf ++ ")"
            arrShow _ tf = tf

            list = intersperse ", " >>> join


mgu :: Type -> Type -> Maybe Subst
mgu Tnum Tnum = Just []
mgu (Tarr a1 r1) (Tarr a2 r2) = do
    s1 <- mgu a1 a2
    mgu (applySubst s1 r1) (applySubst s1 r2)
mgu ()-- TODO resume here


{- we will want to apply substitutions to types -}
type Subst = [(Id, Type)]

class Types t where
    freeVars :: t -> [Id]
    applySubst :: Subst -> t -> t


instance Types Type where
    freeVars = cata alg
        where
            alg (TvarF tv) = [tv]
            alg (TarrF t1 t2) = t1 `union` t2
            alg (ForallF tvs typeF) = typeF \\ tvs
            alg _ = []

    applySubst s = cata alg
        where
            alg TnumF = Tnum
            alg (TvarF tv) =
                case lookup tv s of
                    Just t -> t
                    Nothing -> (Tvar tv)
            alg (TarrF t1 t2) = Tarr t1 t2
            alg (ForallF tvs ty) = Forall tvs ty


instance Types a => Types [a] where
    freeVars = map freeVars >>> foldr union []
    applySubst s = map $ applySubst s


{- applySubst s1 . applySubst s2 == applySubst (s1 @@ s2)-}
(@@) :: Subst -> Subst -> Subst
s1 @@ s2 = s1 ++ []


emptySubst :: Subst
emptySubst = []