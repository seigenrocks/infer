{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

module Tree
    ( TreeZip
    , Tree
    , Error
    , Path
    , hasChild
    , hasParent
    , goPath
    , treezip
    , children
    , leftSiblings
    , rightSiblings
    , below
    , down
    , up
    , prev
    , next
    , toRoot
    , toTip
    )
    where

import Control.Arrow
import Control.Applicative
import Control.Monad
import Control.Monad.Except
import Data.Either
import Data.List
import Data.Maybe

import Test.QuickCheck hiding (label)


data Dir = Up | Down | Prev | Next deriving Show


type Path = [Dir]


data Tree a = Node a [Tree a]
    deriving (Eq, Show)


data TreeZip a =
    TreeZip { label :: !a
            , children :: [Tree a]
            , parents :: [([Tree a], a, [Tree a])]
            }
            deriving (Eq, Show)


instance Arbitrary Dir where
    -- These frequencies are tuned to try to make the cursor often end up
    -- somewhere in the middle of the tree.
    arbitrary = frequency
        [ (1, return Up)
        , (4, return Down)
        , (2, return Prev)
        , (20, return Next)
        ]


instance Arbitrary (Tree Int) where
    -- Scale the size parameter of the generator down each level,
    -- otherwise we get trees that are several Gb.
    arbitrary = liftM2 Node (resize 1000 arbitrary) $ arbitraryChildren


instance Arbitrary (TreeZip Int) where
    -- The resize of the path here is tuned reduce the number of times that
    -- a path ends up in the place that it started.
    arbitrary = liftM goPath arbitrary <*> liftM toZip arbitrary


data Error a = ZipError Dir (TreeZip a)


instance Show a => Show (Error a)
    where
        show (ZipError d t) = "Cannot go " ++ show d ++ " from " ++ show t


arbitraryChildren :: Gen [Tree Int]
arbitraryChildren = do
    n <- getSize
    if n <= 0
    then return []
    else do
        k <- frequency $ [(1, return 1)] ++ [(4, return x) | x <- [2..5]]
        m <- chooseInt (0, n `div` 2)
        resize m $ arbitraryList k


arbitraryList :: Arbitrary a => Int -> Gen [a]
arbitraryList = flip replicateM $ arbitrary


zipIfValid :: (TreeZip a -> Either (Error a) (TreeZip a)) -> TreeZip a -> TreeZip a
zipIfValid f t = either (const t) id $ f t


goDir :: Dir -> TreeZip a -> TreeZip a
goDir Up = zipIfValid up
goDir Down = zipIfValid down
goDir Prev = zipIfValid prev
goDir Next = zipIfValid next


leftSiblings :: TreeZip a -> [Tree a]
leftSiblings TreeZip {parents=[]} = []
leftSiblings TreeZip {parents=(l, _, _):_} = l


rightSiblings :: TreeZip a -> [Tree a]
rightSiblings TreeZip {parents=[]} = []
rightSiblings TreeZip {parents=(_, _, r):_} = r


goPath :: Path -> TreeZip a -> TreeZip a
goPath [] t = t
goPath (d:ds) t = goPath ds (goDir d t)


toZip :: Tree a -> TreeZip a
toZip (Node label children) = TreeZip { label = label, children = children, parents = []}


treezip :: a -> TreeZip a
treezip l = TreeZip { label = l, children = [], parents = []}


below :: TreeZip a -> a -> TreeZip a
below t new = TreeZip { label = l, children = newc:cs, parents = ps }
    where
    TreeZip { label = l, children = cs, parents = ps } = t
    newc = Node new []


down :: MonadError (Error a) m => TreeZip a -> m (TreeZip a)
down t@(TreeZip {children=[]}) = throwError $ ZipError Down t
down t =
    return $ TreeZip { label = l', children = cs', parents = ps' }
    where
        TreeZip { label = l, children = c:cs, parents = ps } = t
        (Node l' cs') = c
        ps' = ([], l, cs):ps


up :: MonadError (Error a) m => TreeZip a -> m (TreeZip a)
up t@(TreeZip {parents=[]}) = throwError $ ZipError Up t
up t =
    return $ TreeZip { label = l', children = cs', parents = ps' }
    where
        TreeZip { label=l, children=cs, parents=p:ps } = t
        (left, l', right) = p
        cs' = reverse left ++ (Node l cs):right
        ps' = ps


next :: MonadError (Error a) m => TreeZip a -> m (TreeZip a)
next t@(TreeZip {parents=[]}) = throwError $ ZipError Next t
next t@(TreeZip {parents=((_, _, []):_)}) = throwError $ ZipError Next t
next t =
    return $ TreeZip { label = l', children = cs', parents = ps' }
    where
        TreeZip { label = l, children = cs, parents = p:ps } = t
        (left, pl, (Node l' cs'):right) = p
        p' = ((Node l cs):left, pl, right)
        ps' = p':ps


prev :: MonadError (Error a) m => TreeZip a -> m (TreeZip a)
prev t@(TreeZip {parents=[]}) = throwError $ ZipError Prev t
prev t@(TreeZip {parents=(([], _, _):_)}) = throwError $ ZipError Prev t
prev t =
    return $ TreeZip { label = l', children = cs', parents = ps' }
    where
        TreeZip { label = l, children = cs, parents = p:ps } = t
        ((Node l' cs'):left, pl, right) = p
        p' = (left, pl, (Node l cs):right)
        ps' = p':ps


hasChild :: TreeZip a -> Bool
hasChild = down >>> isRight


hasParent :: TreeZip a -> Bool
hasParent = up >>> isRight


toRoot :: TreeZip a -> TreeZip a
toRoot = iterate (goDir Up) >>> find (not . hasParent) >>> fromJust


toTip :: TreeZip a -> (a -> Bool) -> TreeZip a
toTip = undefined