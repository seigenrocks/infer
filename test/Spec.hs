{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell #-}

import Control.Arrow
import Control.Monad.Except
import Data.Either
import System.IO.Unsafe

import Test.QuickCheck

import Tree


isSame :: (Eq a, Show a) => TreeZip a -> Either (Error a) (TreeZip a) -> Property
isSame t = either (\e -> collect e $ True === True) (\t' -> t' === t)


prop_below_conses_child :: TreeZip Int -> Property
prop_below_conses_child t = childrenAfter === (newChild:childrenBefore)
    where
        childrenBefore = children t
        childrenAfter = children t'
        t' = below t 42
        newChild = head childrenAfter


prop_below_has_child :: TreeZip Int -> Bool
prop_below_has_child t = hasChild $ below t 42


prop_below_down_has_parent :: TreeZip Int -> Bool
prop_below_down_has_parent t = hasParent $ down' $ below t 0
    where
        down' t' = case down t' of Right x -> x


iterateM :: Applicative m => Int -> (a -> m a) -> a -> m a
iterateM 0 _ = pure
iterateM times fn = fn >>> replicateM times >>> fmap last


prop_down_up :: Int -> TreeZip Int -> Property
prop_down_up n t =
    (hasChild t) ==>
        classify (threshold <= nchildren) (show threshold ++ " or more children") $
        classify (threshold <= nreps) (show threshold ++ " or more nexts") $
        isSame t $ downUp t
    where
        downUp t = down t >>= zipSibling >>= up
        nchildren = length $ children t
        nreps = n `mod` nchildren
        zipSibling = iterateM nreps next
        threshold = 3


sib_props ::
    String
    -> (TreeZip Int -> [Tree Int])
    -> (TreeZip Int -> Either (Error Int) (TreeZip Int))
    -> (TreeZip Int -> Either (Error Int) (TreeZip Int))
    -> Int -> TreeZip Int -> Property

sib_props dir1 sel1 zip1 zip2 n t =
    (nDirSibs > 0) ==>
        classify (threshold <= nDirSibs) (show threshold ++ " or more " ++ dir1 ++ " siblings") $
        classify (threshold <= nreps) (show threshold ++ " or more prev+nexts") $
        isSame t $ backAndForth t
    where
        backAndForth t = (iterateM nreps zip1) t >>= (iterateM nreps zip2)
        nDirSibs = length (sel1 t)
        nreps = if nDirSibs == 1 then 1 else 1 + (n `mod` (nDirSibs-1))
        threshold = 2


prop_prev_next :: Int -> TreeZip Int -> Property
prop_prev_next = sib_props "left" leftSiblings prev next


prop_next_prev :: Int -> TreeZip Int -> Property
prop_next_prev = sib_props "right" rightSiblings next prev


prop_zip_root_is_id :: Path -> TreeZip Int -> Property
prop_zip_root_is_id p t =
    classify eitherRoot "either tree is root" $
    classify equal "path ended up at start" $
    toRoot t === toRoot t'
    where
        t' = goPath p t
        isRoot = not . hasParent
        eitherRoot = isRoot t || isRoot t'
        equal = t == t'



-- This bizarre "return []" serves a purpose. See QuickTest docs:
-- https://hackage.haskell.org/package/QuickCheck-2.14.2/docs/Test-QuickCheck.html#g:3
return []
runTests = $quickCheckAll


main :: IO ()
main = runTests >> return ()
