# infer

After reading this paper [1] I'm impressed and I wonder whether we can't do something similar for parsing flatfiles in bioinformatics. To that end, I'm learning natural deduction from [2] to try to understand cut elimination.

Here I try to apply some of that knowledge by writing a program that takes in an expression from simply-typed lambda calculus with let-generalization and output a LaTeX file with deduction trees, showing different steps of type inference and program reduction. This leans heavily on [3-5]

[1] NAJD, Shayan, et al. Everything old is new again: quoted domain-specific languages. In: Proceedings of the 2016 ACM SIGPLAN Workshop on Partial Evaluation and Program Manipulation. 2016. p. 25-36.

[2] https://www.cs.cmu.edu/~fp/courses/99-atp/

[3] JONES, Mark P. Typing haskell in haskell. In: Haskell workshop. 1999.

[4] https://crypto.stanford.edu/~blynn/lambda/hm.html

[5] https://en.wikipedia.org/wiki/Hindley%E2%80%93Milner_type_system
